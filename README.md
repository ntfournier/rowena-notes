# Rowena Notes

Rowena is a fast way to take formatted notes and exported them as HTML using [Jade] template engine.

This project is greatly inspired by [Dilliger] an amazing HTML5 Markdown editor.

## Why Rowena Notes

As a [feminist] I find interesting to name my software with the name of strong women and according to [Wikipedia]:

> Rowena /roʊˈiːnə/ was the daughter of the Anglo-Saxon chief Hengist and a wife of Vortigern, King of the Britons, in British legend. Presented as a beautiful femme fatale, she won her people the Kingdom of Kent.

And Notes is because this software is clearly to take notes.

## Version
0.0

## License
MIT

[Jade]:http://jade-lang.com
[Dilliger]:http://dillinger.io
[feminist]:http://dictionary.reference.com/browse/feminist?s=t
[Wikipedia]:http://en.wikipedia.org/wiki/Rowena
